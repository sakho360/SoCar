/////////////////////// initial global variables ///////////////////////

	 var source_latitude = document.getElementById("source_latitude").value;
     var source_longitude = document.getElementById("source_longitude").value;
     var destination_latitude = document.getElementById("destination_latitude").value;
     var destination_longitude = document.getElementById("destination_longitude").value;

 	source = new google.maps.LatLng(source_latitude, source_longitude ); 
    destination =  new google.maps.LatLng(destination_latitude, destination_longitude);
    markersArray = [];
// route mapping
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
// map initialization.
    map = new google.maps.Map(document.getElementById('map'),{
	center: source,
	zoom: 12
	});
	directionsDisplay.setMap(map);
// markers initialization.
    marker = new google.maps.Marker({
			position: source,
			map: map,
			animation: google.maps.Animation.DROP
	});
	marker_2 = new google.maps.Marker({
			position: destination,
			map: map,
			animation: google.maps.Animation.DROP
	}); 
	var infowindow = new google.maps.InfoWindow({ 
			content:"Source"
	});
	infowindow.open(map,marker);
	var infowindow = new google.maps.InfoWindow({ 
			content:"Destination"
	});
	infowindow.open(map,marker_2); 

	calculateAndDisplayRoute(directionsService, directionsDisplay);

	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    	directionsService.route({
		origin: source,
		destination: destination,
		travelMode: google.maps.TravelMode.DRIVING
		}, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
    });
  }
  