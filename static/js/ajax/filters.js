$(document).ready( function() {

  $("#datepicker").datepicker({
          constrainInput: true,
          showOn: 'button',
          buttonText: 'Filter by date',
          dateFormat: 'yy-mm-dd',

          onSelect: function(dateText, inst) {
            var date= $(this).val();
            var values = {
              'date': date,
            };
            $.ajax({
              url: "/trip/filter",
              type: "GET",
              data: values,
              success:function(html){
                $(".list-div").empty();
                $(".list-div").hide().fadeIn(1000);
                $(".list-div").append(html);
              },
            });
          }
      }).next(".ui-datepicker-trigger").addClass("btn btn-default");

  $("[name='gender-button']").click(function(){
      var values = {
              'date': '',
            };
      $.ajax({
              url: "/trip/filter",
              type: "GET",
              data: 'values',
              success:function(html){
                $(".list-div").empty();
                $(".list-div").hide().fadeIn(1000);
                $(".list-div").append(html);
              },
            });

    });
  });