$(document).ready( function() {

	$("[name='participant-button']").click(function(){
		var object_id = $(this).attr("object");
		var form = ".participant-form"+object_id ;
		var button_value = $(this).val();
		var data =  new FormData($(form).get(0));
		var accept_button = "#accept-request-button"+object_id
		var reject_button = "#reject-request-button"+object_id
		var remove_participant_button = "#remove-participant-button"+object_id
		var trip = $(this).attr("trip");
		var redirect_url = "/trip/request/list/"+trip+"/"
		if(button_value == "Accept") 
       		data.append('value',"_accept");
       	else if(button_value == "Reject")
       		data.append('value',"_reject");
       	else if(button_value == "Remove")
       		data.append('value',"_remove_participant")
		$.ajax({
	            url: redirect_url,
	            type: "POST",
	            data: data,
	            processData: false,
		        contentType: false,
	            success: function(json) {
	            	if (json.success == 0) {
                  		alert("Oops ! something went wrong."); 
                	}
	            	else if (json.success == 1){
					    if(button_value == "Accept") {
					    	$(accept_button).attr('value', 'Request Accepted');
					    	$(reject_button).hide();
					    }
				       	else if(button_value == "Reject"){
				       		$(reject_button).attr('value', 'Request Rejected');
				       		$(accept_button).hide();
				       	}
				       	else{
				       		$(remove_participant_button).attr('value', 'Participant Removed');
				       	}
		                $("input[name=csrfmiddlewaretoken]").val(json.csrf);            	
						}
	            },
	            error: function(response) {
	            	alert("Oops ! something went wrong."); 
	            }
        }); 
	});


});