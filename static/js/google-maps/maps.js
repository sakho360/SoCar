/////////////////////// form elements ///////////////////////}
/////////////////////// initial global variables ///////////////////////

$(document).ready( function() {

 	cairo = new google.maps.LatLng(30.064742,  31.249509); 
    giza =  new google.maps.LatLng(29.976480,  31.131302);
    source = giza ;
	destination = cairo;
    markersArray = [];
    zoom_level = 12
// route mapping
    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
// map initialization.
    map = new google.maps.Map(document.getElementById('map'),{
	center: cairo,
	zoom: zoom_level
	});
	directionsDisplay.setMap(map);
// markers initialization.
    marker = new google.maps.Marker({
			position: cairo,
			map: map,
			draggable:true,
			animation: google.maps.Animation.DROP
	});
	marker_2 = new google.maps.Marker({
			position:giza,
			map: map,
			draggable:true,
			animation: google.maps.Animation.DROP
	}); 
	var infowindow = new google.maps.InfoWindow({ 
			content:"Destination"
	});
	infowindow.open(map,marker);

	var infowindow = new google.maps.InfoWindow({ 
			content:"Source"
	});
	infowindow.open(map,marker_2); 
// markers' listeners
	google.maps.event.addListener(marker, 'dragend', function (event) {
	    destination = marker.getPosition();
	    calculateAndDisplayRoute(directionsService, directionsDisplay);
	    reverseGecoding(destination, "destination")
	});

	google.maps.event.addListener(marker_2, 'dragend', function (event) {
	    source = marker_2.getPosition();
	    calculateAndDisplayRoute(directionsService, directionsDisplay);
	    reverseGecoding(source, "source");
	});

	google.maps.event.addListener(map, 'click', function(event) {        
	
	});

	GelocationService(marker_2);

	// Search box attributes
	// Create the search box and link it to the UI element.
	var input_source = document.getElementById('id_source_area');
	var searchBox_source = new google.maps.places.SearchBox(input_source);

	var input_destination = document.getElementById('id_destination_area');
	var searchBox_destination = new google.maps.places.SearchBox(input_destination);


	// Search box listeners
	
	searchBox_source.addListener('places_changed', function() {
    var places = searchBox_source.getPlaces();

    if (places.length == 0) {
      return;
    }

    // Clear out the old markers.

    // For each place, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    places.forEach(function(place) {
      var icon = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

    // Create a marker for each place.

	marker_2.setPosition(place.geometry.location);
	source = marker_2.getPosition();
    calculateAndDisplayRoute(directionsService, directionsDisplay);
    reverseGecoding(source, "source")



	if (place.geometry.viewport) {
	// Only geocodes have viewport.
	bounds.union(place.geometry.viewport);
	} else {
	bounds.extend(place.geometry.location);
	}
    });
    map.fitBounds(bounds);
    map.setZoom(11);

  	});


	searchBox_destination.addListener('places_changed', function() {
	    var places = searchBox_destination.getPlaces();

	    if (places.length == 0) {
	      return;
	    }

	    // Clear out the old markers.

	    // For each place, get the icon, name and location.
	    var bounds = new google.maps.LatLngBounds();
	    places.forEach(function(place) {
	      var icon = {
	        url: place.icon,
	        size: new google.maps.Size(71, 71),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(17, 34),
	        scaledSize: new google.maps.Size(25, 25)
	      };

	    // Create a marker for each place.

		marker.setPosition(place.geometry.location);
		destination = marker.getPosition();
	    calculateAndDisplayRoute(directionsService, directionsDisplay);
	    reverseGecoding(destination, "destination")



		if (place.geometry.viewport) {
		// Only geocodes have viewport.
		bounds.union(place.geometry.viewport);
		} else {
		bounds.extend(place.geometry.location);
		}
	    });
	    map.fitBounds(bounds);
	    map.setZoom(11);
	  });



});
	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    	directionsService.route({
		origin: source,
		destination: destination,
		travelMode: google.maps.TravelMode.DRIVING
		}, function(response, status) {
			if (status === google.maps.DirectionsStatus.OK) {
				directionsDisplay.setDirections(response);
			} else {
				window.alert('Directions request failed due to ' + status);
			}
    });
  }
  // detailed information.

    function reverseGecoding(location, sd){
    	calculateDistance();
		var lat = location.lat();
		var lng = location.lng();
		var xmlHttp = null;

		//initialize request and send it
		xmlHttp = new XMLHttpRequest();
		xmlHttp.open( "GET", "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyDlkHz8nwNGtbzLgpLU8yGcrG8KIEwv07s", false );
		xmlHttp.send( null );
		//calls a function to process the response that was received from the request
		if(sd == "source")
			ProcessRequestSource(xmlHttp.responseText);
		else
			ProcessRequestDestination(xmlHttp.responseText);
	}

//takes as an input a JSON object containing location details
	function ProcessRequestSource(response) {
		$("#id_source_latitude").attr('value',  marker_2.getPosition().lat());
	    $("#id_source_longitude").attr('value',  marker_2.getPosition().lng());

		//parse json object to a dictionary
		var json_data = JSON.parse(response);

		//get the formatted address from the dictionary
		var lol = json_data.results[0]['formatted_address'];

		//splits the string into fields and store it in an array
		var add = lol.split(',');
		$("#id_source_area").attr('value', add[0]+","+add[1]+","+add[2]);
	}

	function ProcessRequestDestination(response) {
		$("#id_destination_latitude").attr('value', marker.getPosition().lat());
	    $("#id_destination_longitude").attr('value', marker.getPosition().lng());
		//parse json object to a dictionary
		var json_data = JSON.parse(response);

		//get the formatted address from the dictionary
		var lol = json_data.results[0]['formatted_address'];

		//splits the string into fields and store it in an array
		var add = lol.split(',');
		$("#id_destination_area").attr('value', add[0]+","+add[1]+","+add[2]);
	}


	function GelocationService(marker_2) {


		// Try HTML5 geolocation.
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
			var pos = {
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			var infoWindow = new google.maps.InfoWindow({map: map});
			
			var loc = new google.maps.LatLng(pos.lat, pos.lng);
			marker_2.setPosition(loc);
			initial();
			});
		} else {
			alert("----- error -----")			
		}

	}

	function calculateDistance() {
		var service = new google.maps.DistanceMatrixService;
		service.getDistanceMatrix({
			origins: [source],
			destinations: [destination],
			travelMode: google.maps.TravelMode.DRIVING,
			unitSystem: google.maps.UnitSystem.METRIC, // Kilometer, IMPERIAL for miles.
			avoidHighways: false,
			avoidTolls: false
			}, function(response, status) {
					if (status !== google.maps.DistanceMatrixStatus.OK) {
						alert('Error was: ' + status);
					} else {
						$("#id_distance").attr('value',response.rows[0].elements[0].distance.text);
					}
		});
	}

// initial function.
	function initial(){
		source = marker_2.getPosition();
		calculateAndDisplayRoute(directionsService, directionsDisplay);
		reverseGecoding(source, "source");
		reverseGecoding(destination, "destination")
	}

