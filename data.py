import os
from django.http import HttpResponseRedirect
# import apps of needed models.
import allauth
import autofixture
import main
import comments
import postman
from django.contrib.sites.models import Site
from django.contrib.auth.models import User


def delete_all():
    # main.models.Group.objects.all().members.through.objects.all().delete()
    User.objects.all().delete()
    main.models.Friend.objects.all().delete()
    main.models.Group.objects.all().delete()
    main.models.Guest.objects.all().delete()
    main.models.Trip.objects.all().delete()
    main.models.Profile.objects.all().delete()
    main.models.ObjectHitCounter.objects.all().delete()
    main.models.UrlHitCounter.objects.all().delete()
    allauth.socialaccount.models.SocialApp.objects.all().delete()
    allauth.socialaccount.models.SocialApp.sites.through.objects.all().delete()
    comments.models.Comment.objects.all().delete()
    comments.models.Like.objects.all().delete()
    postman.models.Message.objects.all().delete()


def populate_all():
    """
    Poplulation script to initialize all models.
    """
    populate_social_app()
    populate_users()
    populate_profiles()
    populate_trips()
    main.models.ObjectHitCounter.objects.create()


def populate_social_app():
    add_socialapp_site(add_socialapp("facebook", "SoCar", "1703611953231033", "", "70b208cfd1cc4a610386549b1df13634"), Site.objects.get(id=1))


def populate_users():
    # hash password for "TheKey041234"
    bycrpt_password = "pbkdf2_sha256$24000$IBcGC4uHHJtG$Hx6xsqSjKRvudHAPK+QIWQq6jSgMxXfhIP5jFWavH4s="
    autofixture.create('auth.User', 1, field_values={
                     'username': 'JohnDoe',
                     'first_name': 'John',
                     'last_name' : 'Doe',
                     'email': 'johndoe@live.com',
                     'password': bycrpt_password,
                     'is_superuser': True})
    autofixture.create('auth.User', 1, field_values={
                     'username': 'Brainyshameless',
                     'first_name': 'Jane',
                     'email': 'brainy_shameless@hotmail.com',
                     'password': bycrpt_password})
    autofixture.create('auth.User', 1, field_values={
                     'username': 'TheDarkKnight',
                     'first_name': 'aly',
                     'last_name': 'hussien',
                     'email': 'animerite_aly@gmail.com',
                     'password': bycrpt_password})
    autofixture.create('auth.User', 1, field_values={
                     'username': 'Sandrare',
                     'first_name': 'sandra',
                     'last_name': 'eisa',
                     'email': 'indianmedulla@hotmail.com',
                     'password': bycrpt_password})
    autofixture.create('auth.User', 1, field_values={
                    'username': 'Mariammedhat',
                    'first_name': 'mariam',
                    'email': 'medhat_mariam@hotmail.com',
                    'password': bycrpt_password})
    autofixture.create('auth.User', 1, field_values={
                     'username': 'Ahmadthemetwaly',
                     'first_name': 'ahmad',
                     'last_name': 'metwaly',
                     'email': 'metwaly12polo@yahoo.com',
                     'password': bycrpt_password})
    autofixture.create('auth.User', 3, field_values={
                     'password': bycrpt_password})
    print "---- Users populated ----"


def populate_profiles():
    add_profile(User.objects.get(username="JohnDoe"), "profile/john-doe.jpg", "Two giants live in Britain's land, John Doe and Richard Roe, Who always travel hand in hand, John Doe and Richard Roe, Their fee-faw-fums an ancient plan, To smell the purse of an Englishman, And, ecod, they will suck it all they can")
    add_profile(User.objects.get(username="Brainyshameless"), "profile/girl.jpg", "If all mankind were to disappear, the world would regenerate back to the rich state of equilibrium that existed ten thousand years ago. If insects were to vanish, the environment would collapse into chaos. E. O. Wilson")
    add_profile(User.objects.get(username="TheDarkKnight"), "profile/dark-knight.jpg", "I am what Gotham needs me to be.")
    add_profile(User.objects.get(username="Sandrare"), "profile/indian-girl.jpg", "My ancestors include Monahwee, who was one of the leaders in the Red Stick War, which was the largest Indian uprising in history, and Osceola, who refused to sign a treaty with the United States. Joy Harjo.")
    add_profile(User.objects.get(username="Mariammedhat"), "profile/girl-2.jpg", "Until women learn to want economic independence, and until they work out a way to get this independence without denying themselves the joys of love and motherhood, it seems to me feminism has no roots. Crystal Eastman.")
    add_profile(User.objects.get(username="Ahmadthemetwaly"), "profile/water-polo.jpg", "I fell in love with Erica Kane the summer before my freshman year of high school. Like all red-blooded teen American boys, I'd come home from water polo practice and eat a box of Entenmann's Pop'Ems donut holes in front of the TV while obsessively fawning over 'All My Children' and Erica, her clothes, and her narcissistic attitude.")
    print "---- Profiles populated ----"


def populate_trips():
    main.models.ObjectHitCounter.objects.create()
    add_trip("Each City a Rhapsody", "2016-04-30", 30.057794768073823, 31.3292321571289, 30.05508473681143, 31.293282651123036, "Mokhles Al Alfi, Al Manteqah Al Oula, Nasr City", "El-Nasr Rd, Al Abageyah, Qism El-Khalifa")
    add_trip("Off and Running ", "2016-05-01", 30.080613317482147, 31.33129699365236, 30.12533837395043, 31.3626338474121, "Nabil Al Wakkad, Al Golf, Nasr City", "El-Nasr Rd, Al Abageyah, Qism El-Khalifa")
    add_trip("Gutsy Girl Trip", "2016-05-01", 30.013238800000003, 31.471719699999994, 30.064742, 31.24950899999999, "Mohammed Nagib Axis, Cairo Governorate, Egypt", "Emtedad Ahmed Helmi, Al Fagalah, Al Azbakeyah")
    # add_trip("Loose Gravel ", "2016-04-30", 30.056038822848706,31.36872287382812,30.083756625253923,31.24676241796874, "Dr Mahmoud Saleh Abou Zaid, Al Asherah, Nasr City","Atfet Masgid Moustafa Khalil, Al Barad, As Sahel")
    # add_trip("All Systems Whoa", "2016-05-05", 30.007887497675956,31.4885425149414,30.09563891012963,31.292767666992177, "Life View Compound, Cairo Governorate, Egypt","Martial Passage, AZ Zaytoun Al Qebleyah, El-Zaytoun")
    # add_trip("Here Goes Nothing", "2016-05-8", 30.056038822848706,31.36872287382812,30.083756625253923,31.24676241796874, "Dr Mahmoud Saleh Abou Zaid, Al Asherah, Nasr City","Atfet Masgid Moustafa Khalil, Al Barad, As Sahel")
    # add_trip("Miles and Smiles Away", "2016-04-27", 30.057794768073823,31.3292321571289,30.05508473681143,31.293282651123036, "Mokhles Al Alfi, Al Manteqah Al Oula, Nasr City","El-Nasr Rd, Al Abageyah, Qism El-Khalifa")
    # add_trip("Gypsy Girl Travels", "2016-04-26", 30.01918435287325,31.510171848437494,29.969911516016953,31.331219815429677, "South Teseen, Cairo Governorate, Egypt","Cairo - Sweis Rd, Maadi as Sarayat Al Gharbeyah, Al Maadi")
    print "---- Trips populated ----"


def add_socialapp(provider, name, client_id, key, secret):
    socialapp = allauth.socialaccount.models.SocialApp.objects.get_or_create(
        provider=provider, name=name,
        client_id=client_id, key='', secret=secret)[0]
    print "---- Social app created ----"
    return socialapp


def add_socialapp_site(socialapp, site):
    socialapp_site = allauth.socialaccount.models.SocialApp.sites.through.objects.get_or_create(
        socialapp=socialapp, site=site)
    return socialapp_site


def add_profile(user, avatar, bio):
    profile = main.models.Profile.objects.get_or_create(user=user, avatar=avatar, bio=bio)
    return profile


def add_trip(name, date, source_latitude, source_longitude, destination_latitude, destination_longitude, source_area, destination_area):
    autofixture.create('main.Trip', 1, field_values={
                     'name': name,
                     'date': date,
                     'source_latitude' : source_latitude,
                     'source_longitude': source_longitude,
                     'destination_latitude': destination_latitude,
                     'destination_longitude': destination_longitude,
                     'source_area': source_area,
                     'destination_area': destination_area,
                     'guests':0,
                     })


def test_data(request):
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'myproject.settings')
    # Delete all
    delete_all()
    populate_all()
    return HttpResponseRedirect("/")
