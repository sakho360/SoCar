from __future__ import unicode_literals
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.db import models
from comments.models import Comment
from django.contrib.contenttypes.fields import GenericRelation
from ckeditor.fields import RichTextField
from allauth.socialaccount.models import SocialAccount
# from helpers import dismiss_trip
# Create your models here.


class NameModel(models.Model):
    """
    Base model for name and slug.
    """
    name = models.CharField(max_length=256)
    slug = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(NameModel, self).save(*args, **kwargs)

    def __unicode__(self):
        if self.name:
            return self.name
        else:
            return str(super(NameModel))

    class Meta:
        abstract = True


class Community(NameModel):

    """ SoCar Closed Community."""
    created_by = models.ForeignKey(User, related_name="author")
    members = models.ManyToManyField(User, related_name="community_members")

    def __unicode__(self):
        return self.name


class Trip(NameModel):
    """
    Trips.

    """
    # class Meta:
    # unique_together = (('name', 'user'),)  # composite primary key.

    GUEST_CHOICES = (
        (1, '1 Guest'),
        (2, '2 Guests'),
        (3, '3 Guests'),
        (4, '4 Guests'),
    )
    driver = models.ForeignKey(User, related_name="driver")
    guests = models.IntegerField(choices=GUEST_CHOICES, default=1)
    date = models.DateField()
    time = models.TimeField()
    # google maps attributes
    source_latitude = models.CharField(max_length=256, null=False)
    source_longitude = models.CharField(max_length=256, null=False)
    destination_latitude = models.CharField(max_length=256, null=False)
    destination_longitude = models.CharField(max_length=256, null=False)
    source_area = models.CharField(max_length=128, default='')
    destination_area = models.CharField(max_length=128, default='')
    distance = models.CharField(max_length=256, null=True)
    restrict_to_women = models.BooleanField(default=0)

    # permissions
    communities = models.ManyToManyField(Community, blank=True)
    # restrict_to_women = models.BooleanField(default=False)

    def relevant_group(self):
        return Group.objects.get(trip_id=self.id)

    def retrieve_guest_list(self):
        # retrieve guests relative to trip
        guest_list = Guest.objects.filter(trip=self, accepted=True).values_list('guest', flat=True)
        user_list = []
        for entry in guest_list:
            user = User.objects.get(id=entry)
            user_list.append(user)
        return user_list

    def all_participants(self):
        return Guest.objects.filter(trip_id=self.id, accepted=True).values_list('guest', flat=True)

    def delete(self, *args, **kwargs):
        try:
            # send conclusion email
            # dismiss_trip(self)
            # remove associated guests
            Guest.objects.filter(trip_id=self.id).delete()
            #
            group = Group.objects.get(trip_id=self.id)
            # remove group members
            group.members.through.objects.filter(group=group).delete()
            # remove respective group
            group.delete()
            # remove trip
        except:
            pass
        super(Trip, self).delete(*args, **kwargs)


class Guest(models.Model):
    """
    Trip Guests.
    """
    guest = models.ForeignKey(User, related_name="guest")
    trip = models.ForeignKey(Trip)
    accepted = models.BooleanField(default=False)


class Friend(models.Model):
    """
    Friend Requests.
    """
    sender = models.ForeignKey(User, related_name="sender")
    receiver = models.ForeignKey(User, related_name="receiver")
    accepted = models.BooleanField(default=False)


class Group(NameModel):

    """ SoCar Trip Groups """
    trip = models.OneToOneField(Trip, related_name="trip")
    created_by = models.ForeignKey(User, related_name="created_by")
    members = models.ManyToManyField(User, related_name="members")
    # members = models.CharField(max_length=256)
    comments = GenericRelation(Comment)

    def __unicode__(self):
        return self.name


class Location(models.Model):
    area = models.CharField(max_length=256)
    longitude = models.CharField(max_length=256)
    latitude = models.CharField(max_length=256)
    # bounded radius.
    upper_bound = models.CharField(max_length=256)
    lower_bound = models.CharField(max_length=256)


class Profile(models.Model):
    """

    User's Profile

    To do: CHOICES are to be modified.
    """
    GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )

    CAR_NO_CAR_CHOICES = (
        ('No Car', 'No Car'),
        ('Has Car', 'Has Car'),
    )
    user = models.OneToOneField(User)
    avatar = models.ImageField(upload_to='profile/', null=True, blank=True)
    bio = RichTextField(null=True, blank=True)
    phone_number = models.CharField(max_length=256, null=True, blank=True)
    slug = models.CharField(max_length=256)
    car = models.CharField(
        max_length=256, choices=CAR_NO_CAR_CHOICES, default="Has Car")
    gender = models.CharField(
        max_length=256, choices=GENDER_CHOICES, default="Male")
    location = models.ForeignKey(Location, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.user.username)
        super(Profile, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.user.username


class Feedback(models.Model):
    """
    Model stores visitor's/user's feedback.
    """
    feedback = models.CharField(max_length=512)
    session = models.CharField(max_length=256)
    user = models.ForeignKey(User, null=True)


class UrlHitCounter(models.Model):
    """
    Counts the number of visits for the following urls eg. index page.
    """
    session_key = models.CharField(max_length=256, null=True)
    index = models.IntegerField(default=1)


class ObjectHitCounter(models.Model):
    """
    Counts the number of hits for the following models eg.main.Trip, trips_created.
    """
    sign_up_forms = models.IntegerField(default=0)
    trips_created = models.IntegerField(default=0)
    groups_created = models.IntegerField(default=0)


def get_social_account(self):
    # Fetch user's relative social account
    return SocialAccount.objects.get(user_id=self.id, provider='facebook')


def get_profile(self):
    # Fetch user's relative profle
    return Profile.objects.get(user_id=self.id)


def get_gender(self):
    gender = Profile.objects.get(user_id=self.id).gender
    if gender == "Male":
        return 0
    else:
        return 1

# Add function to auth/User model
User.add_to_class('get_social_account', get_social_account)
User.add_to_class('get_profile', get_profile)
User.add_to_class('get_gender', get_gender)
