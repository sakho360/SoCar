from django.contrib import admin
import models

# For developping purposes.
admin.site.register(models.Friend)
admin.site.register(models.Trip)
admin.site.register(models.Group)
admin.site.register(models.Guest)
